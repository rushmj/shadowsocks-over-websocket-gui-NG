const electron = require('electron');
const log4js = require('log4js');
const TCPRelay = require('shadowsocks-over-websocket').TCPRelay;
const path = require('path')

const {
  app,
  ipcMain,
  BrowserWindow,
  dialog
} = electron;

const DEBUG = false;

let win;
var logger = log4js.getLogger('ss-over-ws-gui');
var running = false;
var relay = null;


 /*Setting the icon property when creating the BrowserWindow only has an effect on Windows and Linux.

To set the icon on OS X, you can use electron-packager and set the icon using the --icon switch.

It will need to be in .icns format for OS X. There is an online icon converter which can create this file from your .png.*/
function createWindow() {

  win = new BrowserWindow({
    width: DEBUG ? 520 : 320,
    height: 500,
    resizeable: false,
    maximizable: false,
    icon: path.join(__dirname,'assets/icons/png/16x16.png'),
    fullscreen: false,
    fullscreenable: false,
    //titleBarStyle: 'hidden',
    //frame: false,
    webPreferences: {
      devTools: DEBUG
    }
  });

  win.loadURL(`file://${__dirname}/assets/html/index.html`);
  win.on('closed', () => {
    win = null;
  });
  // 打开窗口的调试工具
  win.webContents.openDevTools();
  win.show();
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  console.log('window all closed')
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', (event) => {
  console.log('app active')
  if (win === null) {
    createWindow();
  }
});

ipcMain.on('app-start', function(event, config) {
  relay = new TCPRelay(config, true);
  relay.bootstrap().then(function() {
    logger.info('tcprelay is running', config);
    running = true;
    event.sender.send('sslocal-status-change', true);
  }).catch(function(error) {
    logger.error(error);
    running = false;
    event.sender.send('sslocal-status-change', false);
  });
});

ipcMain.on('app-shutdown', function(event) {
  relay && relay.stop().then(function() {
    logger.info('tcprelay is stopped');
    running = false;
    event.sender.send('sslocal-status-change', false);
    relay = null;
  });
});

ipcMain.on('app-index-init', function(event) {
    console.log('app-index-init');
    if(running){
      event.sender.send('sslocal-status-change', true);
    }
});